package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

// Struct de datos que se manejaran dentro del servidor
type Datos struct {
	Materias map[string]map[string]float64
	Alumnos  map[string]map[string]float64
}

var d Datos

func (d *Datos) PromedioTodos() string {
	var reply string = "<table><tr><th>Alumno</th><th>Promedio</th></tr>"
	if d.Alumnos == nil {
		return "<br>No hay materias ni alumnos regitrados aún. <br>Intente más tarde."
	}
	promAlumnos := []float64{}
	for alumno, alumCal := range d.Alumnos {
		i := 0.0
		promedio := 0.0
		for _, calificacion := range alumCal {
			promedio += calificacion
			i++
		}
		promedio = promedio / i
		promAlumnos = append(promAlumnos, promedio)
		reply += fmt.Sprint("<tr><td>", alumno, "</td><td>", promedio, "</td></tr>")
	}
	reply += "</table>"
	promGeneral := 0.0
	i := 0.0
	for _, v := range promAlumnos {
		promGeneral += v
		i++
	}
	if i != 0 {
		promGeneral = promGeneral / i
	}
	reply += fmt.Sprintln("<br><h3>Promedio general: ", promGeneral, "</h3>")
	return reply
}

func (d *Datos) PromedioAlumno(alumno string) string {
	var reply string = "<h2>" + alumno + "</h2><br><table><tr><th>Materia</th><th>Promedio</th></tr>"
	if v, ok := d.Alumnos[alumno]; ok {
		var promedio float64
		i := 0.0
		for materia, calificacion := range v {
			reply += fmt.Sprintln("<tr><th>", materia, "</th><th>", calificacion, "</th></tr>")
			promedio += calificacion
			i++
		}
		reply += "</table>"
		promedio = promedio / i
		reply += fmt.Sprintln("<br><h3>Promedio: ", promedio, "</h3>")
		return reply
	}
	return "<h3>El alumno no existe.<br> Intente con otro nombre.</h3>"
}

func (d *Datos) PromedioMateria(materia string) string {
	var reply string = "<h2>" + materia + "</h2><br><table><tr><th>Alumno</th><th>Promedio</th></tr>"
	if v, ok := d.Materias[materia]; ok {
		var promedio float64
		i := 0.0
		for alumno, calificacion := range v {
			reply += fmt.Sprintln("<tr><th>", alumno, "</th><th>", calificacion, "</th></tr>")
			promedio += calificacion
			i++
		}
		reply += "</table>"
		promedio = promedio / i
		reply += fmt.Sprintln("<br><h3>Promedio: ", promedio, "</h3>")
		return reply
	}
	return "<h3>La materia no existe en la base de datos.<br> Intente con otro nombre de materia.</h3>"
}

// Función que carga el HTML al WebServer
func cargarHtml(dir string) string {
	html, _ := ioutil.ReadFile(dir)
	return string(html)
}

// Función para inicializar el index
func index(res http.ResponseWriter, req *http.Request) {
	res.Header().Set(
		"Content-Type",
		"text/html;charset=UTF-8",
	)
	fmt.Fprintf(
		res,
		cargarHtml("index.html"),
	)
}

// Función para agregar alumno a la base de datos
func agregar(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "POST":
		if err := req.ParseForm(); err != nil {
			fmt.Fprintf(res, "ParseForm() error %v", err)
			return
		}
		// Verifica si la materia ya existe en el diccionario de datos
		if v, ok := d.Materias[req.FormValue("Materia")]; ok {
			// En caso de existir la materia, se busca si existe el alumno.
			if _, ok_2 := v[req.FormValue("Alumno")]; ok_2 {
				// Fracaso en la operación si ya existe el alumno
				res.Header().Set(
					"Content-Type",
					"text/html;charset=UTF-8",
				)
				fmt.Fprintf(
					res,
					cargarHtml("error.html"),
					"El alumno que intentas ingresar, ya existe en la base de datos.<br>Intente con otro alumno.",
				)
				return
			}
		}
		// Éxito en la operación, se registra en el map de Materias
		if d.Materias[req.FormValue("Materia")] == nil {
			d.Materias[req.FormValue("Materia")] = map[string]float64{}
		}
		d.Materias[req.FormValue("Materia")][req.FormValue("Alumno")], _ = strconv.ParseFloat(req.FormValue("Calificacion"), 64)
		// Éxito en la operación, se registra en el map de Alumnos
		if d.Alumnos[req.FormValue("Alumno")] == nil {
			d.Alumnos[req.FormValue("Alumno")] = map[string]float64{}
		}
		d.Alumnos[req.FormValue("Alumno")][req.FormValue("Materia")], _ = strconv.ParseFloat(req.FormValue("Calificacion"), 64)
		respuesta := fmt.Sprint("Alumno: ", req.FormValue("Alumno"), "<br>Materia: ", req.FormValue("Materia"), "<br>Calificación: ", req.FormValue("Calificacion"))
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("add.html"),
			respuesta,
		)
	case "GET":
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("index.html"),
		)
	}
}

//
func alumno(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "POST":
		if err := req.ParseForm(); err != nil {
			fmt.Fprintf(res, "ParseForm() error %v", err)
			return
		}
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("table.html"),
			d.PromedioAlumno(req.FormValue("Alumno")),
		)
	case "GET":
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("index.html"),
		)
	}
}

func todos(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "POST":
		if err := req.ParseForm(); err != nil {
			fmt.Fprintf(res, "ParseForm() error %v", err)
			return
		}
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("table.html"),
			d.PromedioTodos(),
		)
	case "GET":
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("index.html"),
		)
	}
}

//
func materia(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "POST":
		if err := req.ParseForm(); err != nil {
			fmt.Fprintf(res, "ParseForm() error %v", err)
			return
		}
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("table.html"),
			d.PromedioMateria(req.FormValue("Materia")),
		)
	case "GET":
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("index.html"),
		)
	}
}

//
func respaldo(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "POST":
		var r string
		fout, err := os.Create("respaldo.txt")
		if err != nil {
			r += fmt.Sprintln("Error al crear el archivo: ", err.Error())
		}
		err = json.NewEncoder(fout).Encode(d)
		if err != nil {
			r += fmt.Sprintln("Error al codificar el JSON: ", err.Error())
		}
		fout.Close()
		if r == "" {
			r = "<h3>¡Información respaldada correctamente!</h3>"
		}
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("table.html"),
			r,
		)
	case "GET":
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("index.html"),
		)
	}
}

//
func carga(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "POST":
		var r string
		fin, err := os.Open("respaldo.txt")
		if err != nil {
			r += fmt.Sprintln("Error al abrir el archivo: ", err.Error())
		}
		err = json.NewDecoder(fin).Decode(&d)
		if err != nil {
			r += fmt.Sprintln("Error de conversión: ", err.Error())
		}
		fin.Close()
		if r == "" {
			r = "<h3>¡Información cargada correctamente!</h3>"
		}
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("table.html"),
			r,
		)
	case "GET":
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("index.html"),
		)
	}
}

//
func vaciar(res http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "POST":
		d.Materias = make(map[string]map[string]float64)
		d.Alumnos = make(map[string]map[string]float64)
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("table.html"),
			"<h3>Se ha vaciado la base de datos.</h3>",
		)
	case "GET":
		res.Header().Set(
			"Content-Type",
			"text/html;charset=UTF-8",
		)
		fmt.Fprintf(
			res,
			cargarHtml("index.html"),
		)
	}
}

// Main principal
func main() {
	d = Datos{
		Materias: map[string]map[string]float64{},
		Alumnos:  map[string]map[string]float64{},
	}
	// Handle encargado de cargar las hojas de estilo
	http.Handle("/", http.FileServer(http.Dir("css/")))
	// Handle de la función Index
	http.HandleFunc("/index", index)
	http.HandleFunc("/agregar", agregar)
	http.HandleFunc("/alumno", alumno)
	http.HandleFunc("/todos", todos)
	http.HandleFunc("/materia", materia)
	http.HandleFunc("/respaldo", respaldo)
	http.HandleFunc("/carga", carga)
	http.HandleFunc("/vaciar", vaciar)
	fmt.Println("Corriendo servirdor...")
	// Listener del servidor
	http.ListenAndServe(":8080", nil)
}
